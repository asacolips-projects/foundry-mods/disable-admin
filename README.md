# Disable Admin

## Installation
Install using the foundry module browser using the manifest URL: [https://gitlab.com/asacolips-projects/foundry-mods/disable-admin/raw/master/module.json](https://gitlab.com/asacolips-projects/foundry-mods/disable-admin/raw/master/module.json)

## Usage
This module will remove all buttons from your world related to configuring players, enabling or disabling modules, or returning to the setup screen.

**DO NOT INSTALL THIS MODULE UNLESS YOU'RE COMFORTABLE ADMINISTRATING YOUR SERVER MANUALLY**

If you need to remove the module and can't exit your world, you can delete the module files and restart your demo server, but you should not install this module unless you already have a system in place to handle automatic restarts to your server.

## License
MIT. Originally forked from [Gabba Gabba Hey](https://gitlab.com/anathemamask/gabbagabbahey/-/tree/master).